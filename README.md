
## EthRental.sol

ERC20 token to be used to exchange Ether for tokens and tokens for Ether

---

## RentalAgreement.sol

Smart Contract template to be deployed by tenants

---

## Notifier.sol

Smart Contract to receive realtime notifications of all the smart contracts deployed by tenants